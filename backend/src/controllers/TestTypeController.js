const config = require("config.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const salt = bcrypt.genSaltSync(10);
const TestType = require("src/models/TestType");

module.exports = {
  async store(req, res, next) {
    try {
      const type = await TestType.create(req.body);
      return res.json(type);
    } catch (error) {
      return next(error);
    }
  },

  async update(req, res, next) {
    try {
      const type = await TestType.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true },
        function(err, doc) {
          if (err) return next(err);
        }
      );
      if (!type) return next("Registro não encontrado");
      return res.send(type);
    } catch (error) {
      return next(error);
    }
  },

  async list(req, res) {
    const types = await TestType.find();
    return res.json(types);
  },

  async get(req, res) {
    const type = await TestType.findOne({ _id: req.params.id });
    return res.json(type);
  },

  async delete(req, res, next) {
    try {
      await TestType.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err) return next(err);
        if (data.deletedCount == 0) return next("Registro não localizado!");
      });
      return res.json({ message: "Registro excluído com sucesso!" });
    } catch (error) {
      return next(error);
    }
  }
};
