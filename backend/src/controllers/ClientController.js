const config = require("config.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const salt = bcrypt.genSaltSync(10);
const Client = require("src/models/Client");

module.exports = {
  async authenticate(req, res, next) {
    const { email, password } = req.body;
    if (!email) return next("Informe o email");
    if (!password) return next("Informe a senha!");
    try {
      const client = await Client.findOne({ email });
      if (!client) return next("Usuário Não encontrado");

      if (bcrypt.compareSync(password, client.password)) {
        const { password, ...data } = client._doc;
        return res.json({
          client: data,
          token: jwt.sign({ sub: client.id }, config.jwt_secret)
        });
      } else {
        return next("Usuário não encontrado");
      }
    } catch (error) {
      return next(error);
    }
  },

  async store(req, res, next) {
    try {
      const clientExists = await Client.findOne({ email: req.body.email });
      if (clientExists) {
        return next("O Email informado já existe!");
      }
      const encriptedPassword = bcrypt.hashSync(req.body.password, salt);
      const client = await Client.create({
        ...req.body,
        password: encriptedPassword
      });
      const { password, ...clientWithoutPassword } = client._doc;
      return res.json(clientWithoutPassword);
    } catch (error) {
      return next(error);
    }
  },

  async update(req, res, next) {
    try {
      const data = req.body;
      if (data.password) {
        data.password = bcrypt.hashSync(data.password, salt);
      }
      const client = await Client.findOneAndUpdate(
        { _id: req.params.id },
        data,
        { new: true },
        function(err, doc) {
          if (err) return next(err);
        }
      );
      if (!client) return next("Registro não encontrado");
      const { password, ...clientWithoutPassword } = client._doc;
      return res.send(clientWithoutPassword);
    } catch (error) {
      return next(error);
    }
  },

  async list(req, res) {
    const clients = await Client.find().select("-password");
    return res.json(clients);
  },

  async get(req, res) {
    const client = await Client.findOne({ _id: req.params.id }).select(
      "-password"
    );
    return res.json(client);
  },

  async delete(req, res, next) {
    try {
      await Client.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err) return next(err);
        if (data.deletedCount == 0) return next("Registro não localizado!");
      });
      return res.json({ message: "Registro excluído com sucesso!" });
    } catch (error) {
      return next(error);
    }
  }
};
