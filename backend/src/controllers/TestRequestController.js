const config = require("config.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const salt = bcrypt.genSaltSync(10);
const TestRequest = require("src/models/TestRequest");

async function checkRequestProblemsStore(request) {
  let params = {
    test: request.test,
    client: request.client,
    execution_date: null
  };
  if (await TestRequest.findOne(params)) {
    return "Já existe uma solicitação deste exame em aberto para este cliente";
  }
  return false;
}

async function checkRequestProblemsUpdate(id, request) {
  let params = {
    _id: { $ne: id },
    test: request.test,
    client: request.client,
    execution_date: null
  };
  if (await TestRequest.findOne(params)) {
    return "Já existe uma solicitação deste exame em aberto para este cliente";
  }
  if (await TestRequest.findOne({ _id: id, execution_date: { $ne: null } })) {
    return "Não é possível Alterar/Excluir solicitações concuídas";
  }
  return false;
}

module.exports = {
  async store(req, res, next) {
    try {
      const problems = await checkRequestProblemsStore(req.body);
      if (problems) {
        return next(problems);
      }
      let testRequest = await TestRequest.create(req.body);
      testRequest = await testRequest
        .populate(["client", { path: "test", populate: "test_type" }])
        .execPopulate();
      return res.json(testRequest);
    } catch (error) {
      return next(error);
    }
  },

  async update(req, res, next) {
    try {
      const problems = await checkRequestProblemsUpdate(
        req.params.id,
        req.body
      );
      if (problems) return next(problems);

      let testRequest = await TestRequest.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true },
        function(err, doc) {
          if (err) return next(err);
        }
      );
      if (!testRequest) return next("Registro não encontrado");
      testRequest = await testRequest.populate("test").execPopulate();
      return res.send(testRequest);
    } catch (error) {
      return next(error);
    }
  },

  async list(req, res) {
    const requests = await TestRequest.find().populate([
      "client",
      { path: "test", populate: "test_type" }
    ]);
    return res.json(requests);
  },

  async listMy(req, res) {
    const requests = await TestRequest.find({ client: req.user.sub }).populate([
      "client",
      { path: "test", populate: "test_type" }
    ]);
    return res.json(requests);
  },

  async get(req, res) {
    const request = await TestRequest.findOne({ _id: req.params.id }).populate([
      "client",
      { path: "test", populate: "test_type" }
    ]);
    return res.json(request);
  },

  async delete(req, res, next) {
    try {
      await TestRequest.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err) return next(err);
      });
      return res.json({ message: "Registro excluído com sucesso!" });
    } catch (error) {
      return next(error);
    }
  }
};
