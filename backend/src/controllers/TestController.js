const config = require("config.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const salt = bcrypt.genSaltSync(10);
const Test = require("src/models/Test");

module.exports = {
  async store(req, res, next) {
    try {
      let test = await Test.create(req.body);
      test = await test.populate("test_type").execPopulate();
      return res.json(test);
    } catch (error) {
      return next(error);
    }
  },

  async update(req, res, next) {
    try {
      let test = await Test.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true },
        function(err, doc) {
          if (err) return next(err);
        }
      );
      if (!test) return next("Registro não encontrado");
      test = await test.populate("test_type").execPopulate();
      return res.send(test);
    } catch (error) {
      return next(error);
    }
  },

  async list(req, res) {
    const tests = await Test.find().populate("test_type");
    return res.json(tests);
  },

  async get(req, res) {
    const test = await Test.findOne({ _id: req.params.id }).populate(
      "test_type"
    );
    return res.json(test);
  },

  async delete(req, res, next) {
    try {
      await Test.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err) return next(err);
        if (data.deletedCount == 0) return next("Registro não localizado!");
      });
      return res.json({ message: "Registro excluído com sucesso!" });
    } catch (error) {
      return next(error);
    }
  }
};
