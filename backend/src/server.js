require("rootpath")();

const express = require("express");
const mongoose = require("mongoose");
const config = require("config.json");
const jwt = require("src/_helpers/jwt");
const errorHandler = require("src/_helpers/error-handler");

const app = express();

mongoose.connect(config.connection_string, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.use(jwt());

const routes = require("./routes");
app.use(express.json());
app.use(routes);
app.use(errorHandler);

const porta = 3333;
app.listen(porta, () => {
  console.log("Servidor escutando na porta " + porta);
});
