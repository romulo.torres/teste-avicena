const mongoose = require("mongoose");

const { ObjectId } = mongoose.Schema.Types;

const TestSchema = new mongoose.Schema({
  test_name: String,
  test_type: {
    type: ObjectId,
    ref: "TestType"
  }
});

module.exports = mongoose.model("Test", TestSchema);
