const mongoose = require('mongoose')

const TestTypeSchema = new mongoose.Schema({
  type_name: String
})

module.exports = mongoose.model('TestType', TestTypeSchema)
