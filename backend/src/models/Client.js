const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);

const ClientSchema = new mongoose.Schema({
  client_name: {
    type: String,
    required: [true, "O nome do cliente é requerido!"]
  },
  email: {
    type: String,
    required: [true, "O email do cliente é requerido!"],
    unique: true,
    validate: {
      validator: function(v) {
        var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailRegex.test(v);
      },
      message: "O email informado não é válido"
    }
  },
  password: {
    type: String,
    required: [true, "A senha do cliente é requerida!"]
  }
});

module.exports = mongoose.model("Client", ClientSchema);
