const mongoose = require("mongoose");

const { ObjectId } = mongoose.Schema.Types;

const TestRequestSchema = new mongoose.Schema({
  execution_date: Date,
  test: {
    type: ObjectId,
    ref: "Test",
    required: [true, "O exame é requerido!"]
  },
  client: {
    type: ObjectId,
    ref: "Client",
    required: [true, "O cliente é requerido!"]
  }
});

module.exports = mongoose.model("TestRequest", TestRequestSchema);
