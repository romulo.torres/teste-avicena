const express = require("express");

const ClientController = require("src/controllers/ClientController");
const TestTypeController = require("src/controllers/TestTypeController");
const TestController = require("src/controllers/TestController");
const TestRequestController = require("src/controllers/TestRequestController");

const routes = express.Router();

routes.get("/check", (req, res) => {
  return res.status(200).json({ status: "ok" });
});

routes.post("/login", ClientController.authenticate);
routes.get("/clients", ClientController.list);
routes.get("/clients/:id", ClientController.get);
routes.post("/clients", ClientController.store);
routes.put("/clients/:id", ClientController.update);
routes.delete("/clients/:id", ClientController.delete);

routes.get("/types", TestTypeController.list);
routes.get("/types/:id", TestTypeController.get);
routes.post("/types", TestTypeController.store);
routes.put("/types/:id", TestTypeController.update);
routes.delete("/types/:id", TestTypeController.delete);

routes.get("/tests", TestController.list);
routes.get("/tests/:id", TestController.get);
routes.post("/tests", TestController.store);
routes.put("/tests/:id", TestController.update);
routes.delete("/tests/:id", TestController.delete);

routes.get("/requests", TestRequestController.list);
routes.get("/requests/my", TestRequestController.listMy);
routes.get("/requests/:id", TestRequestController.get);
routes.post("/requests", TestRequestController.store);
routes.put("/requests/:id", TestRequestController.update);
routes.delete("/requests/:id", TestRequestController.delete);

module.exports = routes;
