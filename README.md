# Teste de Desenvolvimento de Software AVICENA

App e BackEnd para gerenciamento de solicitações de exames offline first.

## Instruções para execução:

### Clonar o repositório:

```
git clone https://gitlab.com/romulo.torres/teste-avicena.git
```

### BACKEND

O sistema Backend foi desenvolvido em NodeJS.

#### Requisitos

- NodeJS (v12.16.1)
- Npm ou Yarn (v6.13.4)

Para instalação das dependências, a partir da raíz do repositório:

```
cd backend
```

Caso utilize yarn:

```
yarn install
```

Caso utilize npm:

```
npm install
```

Foi utilizado um banco de dados MongoDB, a partir do [MongoDB Atlas](https://cloud.mongodb.com "MongoDB Atlas"), Assim, é interessante garantir que seu computador terá acesso ao servidor de banco de dados. Para isso verifique o acesso à porta 27017, utilizada pelo MongoDB. Você pode verificar isso rapidamente acessando o endereço

```
http://portquiz.net:27017
```

Finalmente, para iniciar o servidor backend, execute

Caso utilize yarn:

```
yarn dev
```

Caso utilize npm:

```
npm run dev
```

#### Observações

O sistema está configurado para excutar na porta 3333. Caso precise alterar essa configuração, a alteração deve ser realizada no arquivo `src/server.js`

    const porta = 3333; // Alterar a porta aqui
    app.listen(porta, () => {
      console.log("Servidor escutando na porta " + porta);
    });

## APP

O App foi desenvolvido em NodeJSm utilizando a API Expo.

#### Requisitos

- NodeJS (v12.16.1)
- Npm ou Yarn (v6.13.4)

Para instalação das dependências, a partir da raíz do repositório:

```
cd app
```

Caso utilize yarn:

```
yarn install
```

Caso utilize npm:

```
npm install
```

#### Antes de Iniciar

O aplicativo precisa ser configurado para o IP da máquina e a porta (Padrão 3333) que receberá as requisições. Isso deve ser feito no arquivo

```
[Raiz do App]/src/config/app.json
```

A alteração a ser realizada é na chave `URL_API`

    {
      "URL_API": "http://[SEU IP AQUI]:3333",
      "STORAGE_KEY": "@AVICENA_TEST"
    }

#### Executando

Finalmente, para iniciar o app, é necessário um emulador, ou um dispositivo conectado no computador via cabo de dados, ou ainda um dispositivo com o aplicativo Expo, encontrado nas Lojas de apps

Caso utilize yarn:

```
yarn start
```

Caso utilize npm:

```
npm start
```

O Sistema exibirá uma página de navegador com as opções "Run on anndroid device os emulator" ou "Run on ios devece os emulator". Escolha de acorrdo com seu ambiente de desenvolvimento.

#### Observações

Segue usuário para testes, caso deseje adicionar / alterar dados, é necessário o login inicialmente.

```
email: romulo.torres@gmail.com
senha: 123456
```
