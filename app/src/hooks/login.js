import { useDispatch } from "react-redux";
import Api from "../services/api";
import useLoading from "./loading";
import errorHandler from "../services/errorHandler";
import { Alert } from "react-native";
import useStorage from "./storage";

export default function useLogin() {
  const dispatch = useDispatch();
  const { loadingStart, loadingStop } = useLoading();
  const [saveAuth, getAuth, deleteAuth] = useStorage("AUTH");

  return {
    async login(email, password) {
      try {
        loadingStart();
        const { data } = await Api.post("login", { email, password });
        Api.defaults.headers.common.Authorization = "Bearer " + data.token;
        dispatch({
          type: "LOGIN_CLIENT",
          payload: { ...data, checked: true }
        });
        saveAuth({ ...data, checked: true });

        Alert.alert("ITest diz:", `Bem vindo(a) ${data.client.client_name}`);
      } catch (error) {
        errorHandler(error.response);
      }
      loadingStop();
    },
    async logout() {
      dispatch({ type: "LOGOUT_CLIENT" });
      deleteAuth();
    },
    async check() {
      const auth = await getAuth();
      if (auth) {
        Api.defaults.headers.common.Authorization = "Bearer " + auth.token;
        dispatch({
          type: "LOGIN_CLIENT",
          payload: { ...auth, checked: true }
        });
      } else {
        dispatch({
          type: "LOGOUT_CLIENT"
        });
      }
    }
  };
}
