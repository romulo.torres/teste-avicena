import NetInfo from "@react-native-community/netinfo";
import { useState, useEffect } from "react";

export default function useNet() {
  const [online, setOnline] = useState(true);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      setOnline(state.isConnected);
    });
    return unsubscribe;
  }, []);

  return online;
}
