import Api from "../services/api";
import { useDispatch } from "react-redux";
import errorHandler from "../services/errorHandler";
import useStorage from "./storage";
import useNet from "./net";

export default function useTests() {
  const [store, load, remove] = useStorage("TESTS");
  const dispatch = useDispatch();
  const online = useNet();
  return {
    async getTests() {
      try {
        let tests = [];
        if (online) {
          tests = await Api.get("tests");
          tests = tests.data;
          store(tests);
        } else {
          tests = (await load()) || [];
        }
        dispatch({
          type: "TESTS_LISTED",
          payload: tests
        });
      } catch (error) {
        errorHandler(error);
      }
    }
  };
}
