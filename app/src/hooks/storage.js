import { Alert, AsyncStorage } from "react-native";
import { STORAGE_KEY } from "../config/app.json";

export default function useStorage(key) {
  return [
    async function save(data) {
      try {
        await AsyncStorage.setItem(
          `${STORAGE_KEY}:${key}`,
          JSON.stringify(data)
        );
      } catch (error) {
        Alert.alert(
          "Ocorreu um erro",
          "Erro ao guardar as informações offline. Por favor, entre em contato com o administrador do sistema"
        );
      }
    },
    async function get() {
      try {
        const data = await AsyncStorage.getItem(`${STORAGE_KEY}:${key}`);
        if (data) return JSON.parse(data);
        return null;
      } catch (error) {
        Alert.alert(
          "Ocorreu um erro",
          "Erro ao recuperar as informações offline. Por favor, entre em contato com o administrador do sistema"
        );
      }
    },
    async function remove() {
      try {
        AsyncStorage.removeItem(`${STORAGE_KEY}:${key}`);
      } catch (error) {
        Alert.alert(
          "Ocorreu um erro",
          "Erro ao remover as informações offline. Por favor, entre em contato com o administrador do sistema"
        );
      }
    }
  ];
}
