import Api from "../services/api";
import { useDispatch } from "react-redux";
import errorHandler from "../services/errorHandler";
import { Alert } from "react-native";
import useLoading from "./loading";
import useStorage from "./storage";
import { useNetInfo } from "@react-native-community/netinfo";
import { URL_API } from "../config/app.json";
import useNet from "./net";
import { useEffect } from "react";

export default function useRequests() {
  const dispatch = useDispatch();
  const { loadingStart, loadingStop } = useLoading();
  const [store, load, remove] = useStorage("REQUESTS");
  const [storeOffline, loadOffline, removeOffline] = useStorage(
    "OFFLINE_REQUESTS"
  );
  const online = useNet();

  return {
    async getRequests() {
      try {
        let requests = [];
        if (online) {
          requests = await Api.get("requests/my");
          requests = requests.data;
          store(requests);
        } else {
          requests = (await load()) || [];
          const offlineRequests = (await loadOffline()) || [];
          requests = [...requests, ...offlineRequests];
        }
        dispatch({
          type: "REQUESTS_LISTED",
          payload: requests
        });
      } catch (error) {
        errorHandler(error);
      }
    },
    async createOfflineRequest(data) {
      const requests = (await load()) || [];
      const offlineRequests = (await loadOffline()) || [];
      const reqExists = [...requests, ...offlineRequests].find(
        r => r.test._id === data.test._id && !r.execution_date
      );
      if (reqExists) {
        Alert.alert(
          "ITest diz:",
          "Você já possui uma solicitação em aberto para este exame!"
        );
        return null;
      } else {
        offlineRequests.push(data);
        storeOffline(offlineRequests);
        return data;
      }
    },
    async createRequest(data) {
      try {
        loadingStart();
        const req = { test: data.test._id, client: data.client._id };
        const request = await Api.post("requests", req);
        Alert.alert("ITest diz:", "Registro salvo com sucesso!");
        return request;
      } catch (error) {
        errorHandler(error);
      } finally {
        loadingStop();
      }
    },
    async updateRequest(id, data) {
      try {
        loadingStart();
        const req = { test: data.test._id, client: data.client._id };
        const request = await Api.put(`requests/${id}`, req);
        Alert.alert("ITest diz:", "Registro atualizado com sucesso!");
        return request;
      } catch (error) {
        errorHandler(error);
      } finally {
        loadingStop();
      }
    },
    async simpleSaveRequest(req) {
      try {
        Api.post("requests", req);
      } catch (error) {
        errorHandler(error);
      }
    },
    async removeRequest(id) {
      try {
        loadingStart();
        const request = await Api.delete(`requests/${id}`);
        Alert.alert("ITest diz:", "Registro excluído com sucesso!");
        return request;
      } catch (error) {
        errorHandler(error);
      } finally {
        loadingStop();
      }
    }
  };
}
