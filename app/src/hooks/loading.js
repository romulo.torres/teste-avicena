import { useDispatch } from "react-redux";

export default function useLoading() {
  const dispatch = useDispatch();
  return {
    loadingStart() {
      dispatch({ type: "LOADING_START" });
    },
    loadingStop() {
      dispatch({ type: "LOADING_STOP" });
    }
  };
}
