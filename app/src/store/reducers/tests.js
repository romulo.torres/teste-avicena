const initialState = {
  list: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "TESTS_LISTED":
      return { ...state, list: action.payload };
    default:
      return state;
  }
};

export default reducer;
