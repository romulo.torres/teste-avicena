const initialState = {
  loading: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "LOADING_START":
      return { ...state, loading: state.loading + 1 };
    case "LOADING_STOP":
      return { ...state, loading: Math.max(state.loading - 1, 0) };
    default:
      return state;
  }
};
