const initialState = {
  client: null,
  token: null,
  checked: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN_CLIENT":
      return { ...action.payload };
    case "LOGOUT_CLIENT":
      return { ...initialState, checked: true };
    default:
      return state;
  }
};

export default reducer;
