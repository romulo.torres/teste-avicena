import { combineReducers } from "redux";
import loading from "./loading";
import requests from "./requests";
import tests from "./tests";
import auth from "./auth";

const store = combineReducers({
  requests,
  tests,
  loading,
  auth
});

export default store;
