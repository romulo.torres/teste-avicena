import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import Nav from "./nav";
import "./config/reactotron";
import store from "./store";
import NetInfo from "./components/NetInfo";

export default function App() {
  return (
    <Provider store={store}>
      <>
        <Nav />
        <NetInfo />
      </>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
