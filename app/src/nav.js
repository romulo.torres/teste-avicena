import React, { useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { useSelector } from "react-redux";

import LoginScreen from "./screens/login";
import useLogin from "./hooks/login";
import RequestsScreen from "./screens/requests";
import CreateRequestScreen from "./screens/requests/create";
import { Colors } from "./theme";

function Nav() {
  const Stack = createStackNavigator();
  const { Navigator, Screen } = Stack;
  const { client, checked } = useSelector(state => state.auth);
  const { check } = useLogin();

  useEffect(() => {
    check();
  }, []);

  if (!checked) return null;

  if (!client) {
    return (
      <NavigationContainer>
        <Navigator screenOptions={screenOptions}>
          <Screen name="Login" component={LoginScreen} />
        </Navigator>
      </NavigationContainer>
    );
  }

  return (
    <NavigationContainer>
      <Navigator screenOptions={screenOptions}>
        <Screen name="Minhas Solicitações" component={RequestsScreen} />
        <Screen name="Nova Solicitação" component={CreateRequestScreen} />
        <Screen name="Editar Solicitação" component={CreateRequestScreen} />
      </Navigator>
    </NavigationContainer>
  );
}
const screenOptions = {
  headerStyle: {
    backgroundColor: Colors.primary
  },
  headerTintColor: Colors.light,
  headerTitleStyle: {
    fontWeight: "bold"
  }
};

export default Nav;
