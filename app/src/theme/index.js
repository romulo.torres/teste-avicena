export const Colors = {
  dark: "#000000",
  light: "#ffffff",
  gray: "#e1e2e1",
  darkGray: "#717272",
  lightGray: "#f5f5f6",
  primary: "#26a69a",
  primaryLight: "#64d8cb",
  primaryDark: "#00766c",
  secondary: "#f57f17",
  secondaryLight: "#ffb04c",
  secondaryDark: "#bc5100"
};
