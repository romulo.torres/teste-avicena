import React, { useEffect, useState } from "react";
import { Text, Picker, Alert } from "react-native";
import useTests from "../../hooks/tests";
import { useSelector } from "react-redux";
import Screen from "../../components/Screen";
import Button from "../../components/Button";
import useRequests from "../../hooks/requests";
import {
  NavigationRouteContext,
  useNavigation
} from "@react-navigation/native";
import useNet from "../../hooks/net";

export default function CreateRequestScreen(props) {
  const isUpdate = () => {
    return props.route.params && props.route.params.request;
  };

  const initial = isUpdate() ? props.route.params.request.test._id : null;
  const tests = useSelector(state => state.tests.list);
  const client = useSelector(state => state.auth.client);
  const { getTests } = useTests();
  const {
    getRequests,
    createRequest,
    createOfflineRequest,
    updateRequest
  } = useRequests();
  const [currentTest, setCurrentTest] = useState(initial);
  const navigation = useNavigation();
  const online = useNet();

  useEffect(() => {
    getTests();
  }, []);

  async function handleSubmit() {
    if (!currentTest) {
      Alert.alert("ITest diz:", "Selecione o Exame para a solicitação");
      return;
    }
    const test = tests.find(t => t._id === currentTest);
    let req = null;
    if (isUpdate()) {
      if (online) {
        req = await updateRequest(props.route.params.request._id, {
          test,
          client
        });
      } else {
        Alert.alert(
          "ITests informa:",
          "Não é possível editar solicitações em modo offline."
        );
      }
    } else {
      if (online) {
        req = await createRequest({ test, client });
      } else {
        req = await createOfflineRequest({ test, client });
      }
    }
    if (req) {
      navigation.goBack();
      await getRequests();
    }
  }

  return (
    <Screen>
      <Text>Selecione o exame desejado</Text>
      {tests && (
        <Picker
          selectedValue={currentTest}
          onValueChange={itemValue => setCurrentTest(itemValue)}
        >
          <Picker.Item label={`Selecione`} value={null} key={0} />
          {tests.map(t => (
            <Picker.Item
              label={`${t.test_name} (${t.test_type.type_name})`}
              value={t._id}
              key={t._id}
            />
          ))}
        </Picker>
      )}
      <Button onPress={handleSubmit} primary>
        {isUpdate() ? "ATUALIZAR" : "SOLICITAR"}
      </Button>
    </Screen>
  );
}
