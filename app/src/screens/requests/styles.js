import styled from "styled-components/native";
import Icon from "react-native-vector-icons/Ionicons";
import { Colors } from "../../theme";

export const Box = styled.TouchableOpacity`
  padding: 10px;
  border: 1px solid ${Colors.gray};
`;
export const Title = styled.Text`
  font-size: 18px;
  font-weight: bold;
`;
export const Type = styled.View`
  align-items: flex-start;
  margin-bottom: 3px;
`;
export const TypeText = styled.Text`
  background: ${Colors.primary};
  padding: 2px 10px;
  border-radius: 10px;
  color: ${Colors.light};
`;
export const Execution = styled.Text`
  text-align: right;
  font-size: 15px;
`;

export const ActionIcon = styled(Icon)`
  font-size: 40px;
  color: #fff;
`;

export const ActionButton = styled.TouchableOpacity`
  width: 50px;
  height: 100%;
  background-color: #c20;
  align-items: center;
  justify-content: center;
`;
