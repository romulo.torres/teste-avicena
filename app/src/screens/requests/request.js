import React from "react";
import {
  Box,
  Title,
  Execution,
  Type,
  TypeText,
  ActionIcon,
  ActionButton
} from "./styles";
import moment from "moment";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import useRequests from "../../hooks/requests";
import useNet from "../../hooks/net";

export default function Request(props) {
  const { request } = props;
  const { removeRequest, getRequests } = useRequests();
  const navigation = useNavigation();
  const online = useNet();

  async function handlerDelete() {
    await removeRequest(request._id);
    await getRequests();
  }

  const renderLeftActions = () => {
    return (
      <ActionButton
        onPress={() => {
          if (!online) {
            Alert.alert(
              "ITests diz:",
              "Não é possível remover solicitações em modo offline."
            );
          } else if (request.execution_date) {
            Alert.alert(
              "ITests diz:",
              "Não é possível remover solicitações conclídas."
            );
          } else {
            Alert.alert(
              "ITests pergunta:",
              "Confirma a exclusão da solicitação?",
              [
                {
                  text: "SIM, excluir!",
                  onPress: handlerDelete
                },
                { text: "NÃO" }
              ]
            );
          }
        }}
      >
        <ActionIcon name="md-trash" />
      </ActionButton>
    );
  };

  return (
    <Swipeable renderRightActions={renderLeftActions}>
      <Box
        onPress={() => {
          if (!online) {
            Alert.alert(
              "ITests diz:",
              "Não é possível editar solicitações em modo offline."
            );
          } else if (request.execution_date) {
            Alert.alert(
              "ITests diz:",
              "Não é possível editar solicitações conclídas."
            );
          } else {
            navigation.navigate("Editar Solicitação", { request });
          }
        }}
      >
        <Type>
          <TypeText>{request.test.test_type.type_name}</TypeText>
        </Type>
        <Title>{request.test.test_name}</Title>
        <Execution>
          {request.execution_date
            ? moment(request.execution_date).format("DD/MM/YYYY")
            : "AGUARDANDO "}
          {!request._id && "(OFFLINE)"}
        </Execution>
      </Box>
    </Swipeable>
  );
}
