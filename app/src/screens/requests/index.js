import React, { useEffect } from "react";
import { FloatingAction } from "react-native-floating-action";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { FlatList } from "react-native-gesture-handler";
import { Colors } from "../../theme";
import { Alert } from "react-native";
import useRequests from "../../hooks/requests";
import useLogin from "../../hooks/login";
import Request from "./request";
import Icon from "react-native-vector-icons/Ionicons";
import useNet from "../../hooks/net";
import useStorage from "../../hooks/storage";

export default function RequestsScreen() {
  const requests = useSelector(state => state.requests.list);
  const [storeOffline, loadOffline, removeOffline] = useStorage(
    "OFFLINE_REQUESTS"
  );
  const { getRequests, simpleSaveRequest } = useRequests();
  const { logout } = useLogin();
  const navigation = useNavigation();
  const online = useNet();

  const syncronize = async () => {
    if (online) {
      const offlineRequests = (await loadOffline()) || [];
      for (const data of offlineRequests) {
        const req = {
          test: data.test._id,
          client: data.client._id
        };
        await simpleSaveRequest(req);
      }
      await removeOffline();
      await getRequests();
    }
  };

  useEffect(() => {
    getRequests();
  }, []);

  useEffect(() => {
    syncronize();
  }, [online]);

  return (
    <>
      {requests && (
        <FlatList
          data={requests}
          renderItem={({ item }) => <Request request={item} />}
          keyExtractor={(r, idx) => `${r._id}_${idx}`}
        />
      )}
      <FloatingAction
        color={Colors.secondary}
        actions={[
          {
            text: "Atualizar solicitações",
            color: Colors.secondary,
            icon: <Icon name="md-refresh" style={styles.actionButtonIcon} />,
            name: "reload",
            position: 2
          },
          {
            text: "Nova Solicitação",
            color: Colors.primaryDark,
            icon: <Icon name="md-create" style={styles.actionButtonIcon} />,
            name: "create",
            position: 2
          },
          {
            text: "Logout",
            color: Colors.secondaryDark,
            icon: <Icon name="md-exit" style={styles.actionButtonIcon} />,
            name: "logout",
            position: 1
          }
        ]}
        onPressItem={opt => {
          if (opt === "logout") {
            Alert.alert(
              "ITests pergunta",
              "Tem certeza que deseja sair do aplicativo?",
              [
                {
                  text: "SIM, quero sair",
                  onPress: logout
                }
              ]
            );
          } else if (opt === "create") {
            navigation.navigate("Nova Solicitação");
          } else if (opt === "reload") {
            getRequests();
          }
        }}
      />
    </>
  );
}

const styles = {
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
};
