import styled from "styled-components/native";
import Input from "../../components/Input";
import Constants from "expo-constants";
import Button from "../../components/Button";
import { Colors } from "../../theme";

export const Container = styled.ScrollView`
  padding-top: ${Constants.statusBarHeight}px;
  background-color: ${Colors.lightGray};
  padding-horizontal: 30px;
`;

export const Title = styled.Text`
  flex: 1;
  text-align: center;
  font-size: 20px;
  color: ${Colors.primary};
  font-weight: bold;
  margin-bottom: 25px;
`;

export const Form = styled.View`
  align-self: stretch;
`;

export const FormInput = styled(Input)`
  margin-bottom: 10px;
`;

export const LoginButton = styled(Button)`
  margin-top: 5px;
`;
