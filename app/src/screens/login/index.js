import React, { useRef, useState, useEffect } from "react";
import Brand from "../../components/Brand";
import LoginButton from "../../components/Button";
import Screen from "../../components/Screen";
import useLogin from "../../hooks/login";
import { Form, FormInput, Title } from "./styles";
import { useSelector } from "react-redux";

export default function Login() {
  const senhaRef = useRef();
  const { checked } = useSelector(state => state.auth);
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  const { login } = useLogin();

  function handleSubmit() {
    login(email, senha);
  }

  if (!checked) return null;

  return (
    <Screen>
      <Brand />
      <Form>
        <Title>ITest</Title>
        <FormInput
          onSubmitEditing={() => senhaRef.current.focus()}
          value={email}
          onChangeText={txtEmail => setEmail(txtEmail)}
          icon="email"
          keyboardType="email-address"
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="E-mail"
          returnKeyType="next"
        />
        <FormInput
          onSubmitEditing={handleSubmit}
          value={senha}
          onChangeText={txtSenha => setSenha(txtSenha)}
          icon="lock-outline"
          secureTextEntry
          ref={senhaRef}
          placeholder="Senha"
          returnKeyType="send"
        />
        <LoginButton onPress={handleSubmit} primary>
          ENTRAR
        </LoginButton>
      </Form>
    </Screen>
  );
}
