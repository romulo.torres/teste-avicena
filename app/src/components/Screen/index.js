import React from "react";
import {
  ScrollView,
  Padding,
  KeyboardAvoidingView,
  SafeAreaView
} from "./styles";

export default function Screen({ children }) {
  return (
    <KeyboardAvoidingView behavior={"padding"}>
      <SafeAreaView>
        <ScrollView keyboardShouldPersistTaps="always">
          <Padding>{children}</Padding>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
}
