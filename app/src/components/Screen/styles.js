import styled from "styled-components/native";

export const Padding = styled.View`
  padding: 10px;
  padding-bottom: 80px;
`;

export const SafeAreaView = styled.SafeAreaView`
  flex: 1;
`;

export const ScrollView = styled.ScrollView``;

export const KeyboardAvoidingView = styled.KeyboardAvoidingView`
  flex: 1;
`;
