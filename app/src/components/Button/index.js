import React from "react";
import { ActivityIndicator } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import { RButton, Row, Text } from "./style";
import { useSelector } from "react-redux";

export default function({ children, icon, ...rest }) {
  const { loading } = useSelector(state => state.loading);
  return (
    <RButton {...rest}>
      {loading ? (
        <ActivityIndicator size="small" color="#fff" />
      ) : (
        <Row>
          {icon && (
            <Icon
              name={icon}
              size={20}
              color="white"
              style={{ marginRight: 5 }}
            />
          )}
          <Text>{children}</Text>
        </Row>
      )}
    </RButton>
  );
}
