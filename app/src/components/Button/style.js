import styled from "styled-components";
import { RectButton } from "react-native-gesture-handler";
import { Colors } from "../../theme";

export const RButton = styled(RectButton)`
  justify-content: center;
  align-items: center;
  height: 46px;
  padding: 10px;
  border-radius: 4px;
  background: ${({ primary }) => (primary ? Colors.primary : Colors.secondary)};
`;

export const Text = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: ${Colors.light};
`;

export const Row = styled.View`
  flex-direction: row;
`;
