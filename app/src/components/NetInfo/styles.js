import styled from "styled-components/native";
import { Colors } from "../../theme";
import { Dimensions } from "react-native";

const width = Dimensions.get("screen").width;

export const Text = styled.Text`
  text-align: center;
  position: absolute;
  bottom: 5px;
  width: 180px;
  background: ${Colors.darkGray};
  color: ${Colors.light};
  border-radius: 10px;
  height: 20px;
  margin-left: ${(width - 180) / 2}px;
`;
