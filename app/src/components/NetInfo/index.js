import React from "react";
import { Text } from "./styles";
import useNet from "../../hooks/net";

// import { Container } from './styles';

export default function NetInfo() {
  const online = useNet();
  if (online) return null;
  return <Text>Você está Offline!</Text>;
}
