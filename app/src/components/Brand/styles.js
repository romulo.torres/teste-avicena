import styled from 'styled-components/native';

export const Logo = styled.Image`
  align-self: center;
  height: 100px;
  width: 250px;
  resize-mode: contain;
  margin-top: 50px;
  margin-bottom: 20px;
`;
