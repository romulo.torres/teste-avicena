import React from "react";
import { Logo } from "./styles";
import loggedLogo from "../../../assets/icon.png";

export default function Brand() {
  return <Logo source={loggedLogo} />;
}
