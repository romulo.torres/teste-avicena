import React, { forwardRef } from "react";
import { Container, TInput, Button, Ico } from "./style";

function Input({ onSubmitEditing, style, icon, iconColor, ...rest }, ref) {
  return (
    <Container style={style}>
      <TInput {...rest} ref={ref} onSubmitEditing={onSubmitEditing} />
      {icon && (
        <Button onPress={onSubmitEditing}>
          <Ico name={icon} size={20} color={iconColor} />
        </Button>
      )}
    </Container>
  );
}

export default forwardRef(Input);
