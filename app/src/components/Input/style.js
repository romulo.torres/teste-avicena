import styled from "styled-components/native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Colors } from "../../theme";

export const Container = styled.View`
  padding: 0 10px 0 0;
  height: 46px;
  background: ${Colors.lightGray};
  border-radius: 4px;
  flex-direction: row;
  align-items: center;
  border: 1px solid ${Colors.primary};
`;

export const TInput = styled.TextInput.attrs(({ phColor }) => ({
  placeholderTextColor: phColor ? phColor : Colors.dark
}))`
  flex: 1;
  font-size: 18px;
  height: 45px;
  padding-left: 10px;
  color: ${({ textColor }) => (textColor ? textColor : Colors.dark)};
`;

export const Ico = styled(Icon)`
  color: ${({ iconColor }) => (iconColor ? iconColor : Colors.primary)};
`;

export const Button = styled.TouchableWithoutFeedback``;
