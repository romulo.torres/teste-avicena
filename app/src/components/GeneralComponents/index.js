import styled from "styled-components";
import Constants from "expo-constants";
import { Colors } from "../../theme";

export const Container = styled.View`
  flex: 1;
  padding-top: ${Constants.statusBarHeight}px;
  background-color: ${Colors.lightGray};
`;

export const Paper = styled.View`
  padding: 10px;
  background-color: ${Colors.lightGray};
  border: 1px solid ${Colors.gray};
  border-radius: 4px;
`;

export const TitlePaper = styled.Text`
  text-align: center;
  background: ${Colors.dark};
  font-size: 18px;
  font-weight: bold;
  color: #000;
  text-transform: uppercase;
  padding: 5px;
`;

export const Row = styled.View`
  flex-direction: row;
`;

export const Lbl = styled.Text`
  font-weight: bold;
  text-align: right;
  width: 50px;
  padding-right: 5px;
`;

export const LongTxt = styled.Text.attrs({
  ellipsizeMode: "tail"
})`
  color: ${Colors.dark};
  flex-shrink: 1;
`;
