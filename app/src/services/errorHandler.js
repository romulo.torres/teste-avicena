import { Alert } from "react-native";

export default function errorHandler(error) {
  try {
    if (error.response && error.response.data) {
      Alert.alert("Ocorreu um erro", error.response.data.errors.join(", "));
    } else if (error.data) {
      Alert.alert("Ocorreu um erro", error.response.data.errors.join(", "));
    } else {
      throw Error();
    }
  } catch (e) {
    Alert.alert(
      "Ocorreu um erro",
      "Entre em contato com o administrador do sistema."
    );
  }
}
