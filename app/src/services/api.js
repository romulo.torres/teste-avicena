import axios from "axios";

import { URL_API } from "../config/app.json";

const Api = axios.create({
  baseURL: URL_API
});

export default Api;
